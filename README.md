# Sorcery Tournament prototype in pico-8

## Play it!

https://eveenova.itch.io/lst-00001?secret=vKv6znvsNYAGYCazmPIjazrCw

![an animated gif showing gameplay](https://cdn.discordapp.com/attachments/708375423251382284/843632946925010974/lst_proto_1.gif)

## Overview

You snuck your way into this year's regional sorcery tournament, but you're not exactly a sorceress... chat with your first competitor beforehand, then make your way over to the magical cat organizing the fight, see how you do, and catch up with your opponent afterward!

## Some features & components

- basic platforming
  - lil extras like sliding, cratering down to the ground, and gliding with a lil parachute
- a full npc+dialogue management system for easy configuration and rearrangement of dialogue trees to play
  - dialogue options and conditional dialogue trees
  - auto-linebreaks!
- scene management
  - only a little bit hardcoded for this demo
- collectibles and projectiles


## resources 

- https://nerdyteachers.com/Explain/Platformer/
