pico-8 cartridge // http://www.pico-8.com
version 32
__lua__
-- lesbian sorcery tournament
-- by evee nova

-- v6

-- TODOs
--[[
    bug: big crater can cause ground stuck
]]

left,right,up,down,btn1,btn2=0,1,2,3,4,5
black,dark_blue,dark_purple,dark_green,brown,dark_gray,light_gray,white,red,orange,yellow,green,blue,indigo,pink,peach=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
lyr_fall = 0
lyr_ceil = 1
lyr_wall = 2

sfx_mana = 0
sfx_sel = 1
sfx_adv_sel = 2
sfx_adv = 3
sfx_crater = 4
sfx_hurt = 5
sfx_jump = 6
sfx_spell = 7
sfx_sword = 8

function _init()
    t = 0 -- global slo bounce anim time
    bo = 0 -- global slo bounce offset
    t_f = 0 -- global fast bounce anim time
    bo_f = 0 -- global fast bounce offset
    g = 0.3
    bound_l = 0 -- map boundary left
    bound_r = 128 -- map boundary right
    map_x = 0 -- current scene x
    map_y = 0 -- current scene y

    -- TODO move this to a proper place
    sword = {
        fall = false,
        x=40,
        y=-200,
    }

    state_flags = {notara=true}

    init_dia()

    init_lobby()
end

function init_lobby()
    scene = "lobby"
    map_x = 0
    map_y = 0

    init_player(5, 112)
    init_npcs()

    init_cam()
end

function init_arena()
    scene = "arena"
    map_x = 15
    map_y = 0

    init_player(10, 112)
    init_cam()

    load_scrpt(scripts.tara_intro)
    init_enemy()

    init_spells()
end

function _update()
    if (sword.fall == true) then
        sword.y += 10

        if (sword.y > 70) then
            sword.fall = false
            shake(30)
            sfx(sfx_sword)
            setStates({"tara_win"})
        end
    end
    bounce_offset()
    local prevscene = scene

    update_scene()

    -- scene updates
    if (scene == "lobby") update_lobby()
    if (scene == "arena") update_arena()
end

function update_scene()
    if (hasState("fight")) then
        if (not (scene == "arena")) init_arena()
    elseif (not (scene == "lobby")) then
        init_lobby()
    end
end

function update_lobby()
    update_player()
    animate_player()

    update_npcs()
    animate_npcs()

    update_dia()

    update_cam()
end

function update_arena()

    if (not dia.play) then
        update_player()
        update_enemy()
        update_spells()
    end

    animate_player()

    update_dia()
    update_cam()

    if (hasState("tara_win") or hasState("tara_lose")) then
        if (not hasState("outro")) then
            setStates({"outro"})
            load_scrpt(hasState("tara_win") and scripts.tara_outro_win or scripts.tara_outro_lose);
        end

        if (hasState("outro_finish")) then
            delStates({"outro", "outro_finish", "fight", "notara"})
            init_lobby()
        end
    end
end

function init_enemy()
    enemy = {
        name="tara",
        x=60,
        y=40,
        dx=1,
        dy=0,
        sp = 19,
        sps = {19,20},
        t=0,
        pt = 0, -- projectile timer
        tracks = true,
        flipx = false,
        spn_chnc = 90 -- percent
    }
end

function update_enemy()
    if (enemy.x <= 0 or enemy.x >= 120) then
        enemy.dx = -enemy.dx
    end

    enemy.x += enemy.dx
    enemy.y += enemy.dy

    animate(enemy, enemy.sps[1], enemy.sps[2], .1)

    enemy.pt = step(enemy.pt, 30)
    if (enemy.pt == 0) then
        local midx, midy = enemy.x+4, enemy.y+4
        sfx(sfx_spell)
        add(projs, {
            x=midx+4,
            y=midy+4,
            dx=randb(-1, 1),
            dy=randb(0.3, 1),
            w=3,
            h=3,
            dmg = 30
        })

        if (rnd(100) < enemy.spn_chnc) then
            spawn_mana(midx, midy)
        end
    end
end

function draw_enemy()
    spr(enemy.sp, enemy.x, enemy.y, 1, 1, enemy.flipx)
end

function init_spells()
    proj_t = 0
    projs = {
        -- {
        --     x=100,
        --     y=114,
        --     dx=-1,
        --     dy=0,
        --     w=3,
        --     h=3,
        --     dmg = 10
        -- }
    }
    manas = {
        -- {
        --     x=70,
        --     y=30,
        --     w=4,
        --     h=4,
        --     xins=3,
        --     ttl=400,
        -- }
    }
    is_collide = false
end

function spawn_mana(x, y)
    add(manas, {
        x=x,
        y=y,
        dy=0,
        w=4,
        h=4,
        xins=3,
        ttl=100 + rnd(100),
    })
end

function update_spells()
    for mana in all(manas) do
        -- TODO make x do a lil wave as it falls
        mana.dy += 0.05
        mana.ttl -= 1
        if (map_clisn(mana, down, lyr_fall, mana.xins)) then
            mana.dy = 0
        end

        mana.y += mana.dy

        if (overlap(mana, p)) then
            p.mana += 20
            sfx(sfx_mana)
            del(manas, mana)
        end
        if (mana.ttl == 0) del(manas, mana)
    end

    for proj in all(projs) do
        proj.x += proj.dx
        proj.y += proj.dy

        if (overlap(proj, p)) then
            p.hp -= proj.dmg
            sfx(sfx_hurt)
            del(projs, proj)
        end
    end
end

function draw_spells()
    -- print(is_collide, 0, 0, orange)
    -- print(p.hp, 0, 8, orange)
    -- print(p.mana, 0, 16, orange)
    for mana in all(manas) do
        if (mana.ttl > 30 or bo_f == 0) spr(31, mana.x-mana.xins, mana.y-mana.xins)
    end
    for proj in all(projs) do
        if (bo_f == 0) then
            circfill(proj.x,proj.y,1,red)
        else
            circ(proj.x,proj.y,2,red)
            pset (proj.x, proj.y, red)
        end
    end
end

function _draw()
    cls()

    rectfill(-128,-128,256,256, black)
    srand(1)
    for i=1, 30 do
        pset(rnd(128), rnd(256)-128, white)
    end
    srand()
    map(0, 16)

    -- scene updates
    if (scene == "lobby") draw_lobby()
    if (scene == "arena") draw_arena()

    -- DEBUG STATE FLAGS
    -- for flag,value in pairs(state_flags) do
    --     print(flag)
    -- end

end

function draw_lobby()
    map(map_x, map_y)

    draw_npcs()
    draw_player()

    draw_dia()

    set_cam()
end

function draw_arena()
    map(map_x, map_y)

    draw_enemy()
    draw_player()
    if (not dia.play) draw_player_hud()

    draw_spells()
    draw_dia()

    if (p.mana >= 100) sspr(40, 8, 9, 16, sword.x, sword.y, 50, 100)

    set_cam()
end

function init_cam()
    cam_x = 0
    cam_y = 0
    sh_s = 0 -- shake strength
    sh_r = 0.7 -- shake rate of decay
end

function update_cam()
    sh_s *= sh_r
    if (sh_s < 0.2) sh_s = 0

    cam_x = 0
    cam_y = mid(-1000, p.y-70, 0)
end

function shake(str) -- strength
    sh_s = str
end

function set_cam()
    local sh_x = sh_s - rnd(sh_s * 2)
    local sh_y = sh_s - rnd(sh_s * 2)
    camera(cam_x + sh_x, cam_y + sh_y)
end

function bounce_offset()
    t = (t + 1) % 15
    bo = t == 0 and (bo + 1) % 2 or bo

    t_f = (t_f + 1) % 5
    bo_f = t_f == 0 and (bo_f + 1) % 2 or bo_f

end

-- UTILS gathered from various internet srcs
function randb(low,high)
    return rnd(high-low+1)+low
end

-- time-based stepper
function step(t, s)
    return (t + 1) % s
end

function setStates(keys)
    for i=1,#keys do
        state_flags[keys[i]] = true
    end
end
function delStates(keys)
    for i=1,#keys do
        state_flags[keys[i]] = nil
    end
end
function hasState(key)
    return state_flags[key] ~= nil
end

-- print with outline
function oprint(str, x, y, c0, c1)
    -- background
    for xx = -1, 1 do
        for yy = -1, 1 do
            print(str, x+xx, y+yy, c1)
        end
    end
    -- text
    print(str,x,y,c0)
end

-- lerp
function lerp(a,b,t)
    return a+(b-a)*t
end

-- obj with t and sp, beginning, end, duration between frames
function animate(obj, b, e, d)
    obj.sp = mid(b, obj.sp, e)
    if time() - obj.t > d then
        obj.t = time()
        obj.sp += 1
        if (obj.sp > e) obj.sp = b
    end
end

function overlap(a,b)
    return not (a.x>b.x+b.w
        or a.y>b.y+b.h
        or a.x+a.w<b.x
        or a.y+a.h<b.y)
end

-->8
--player
function init_player(x, y)
    p = {
        hp=100,
        mana=0,
        x = x,
        y = y,
        dx = 0,
        dy = 0,
        max_dx = 2,
        max_dy = 3,
        w = 7,
        h = 7,
        sp = 1,
        acc = 0.5,
        djump = 4,
        gl = 0.3, -- glide factor
        flipx = false,
        wght = 0.3,
        drag = 0.90, -- air friction
        fric = 0.80,
        run = false,
        slide = false,
        fall = false,
        glide = false,
        groundt = false,
        crater = false,
        t=0,
    }
end

function update_player()
    local fric = p.groundt and p.fric or p.drag
    if (btn(down) and p.slide) then
        fric = 0.97
    end
    p.dx = mid(-p.max_dx, p.dx * fric, p.max_dx)
    p.dy = mid(-100, p.dy + p.wght, p.max_dy)

    -- process input
    if (not dia.play) then

        if (not crater and btn(left)) then
            p.dx -= p.acc
            p.flipx = true
            p.run = true
        elseif (not crater and btn(right)) then
            p.dx += p.acc
            p.run = true
            p.flipx = false
        end
        if (btnp(up) and p.groundt) then
            sfx(sfx_jump)
            p.groundt = false
            p.dy -= p.djump
        end
        p.glide = not p.crater and p.fall and btn(up)

        -- TODO must have min altitude -- else transition into slide
        if ((not p.groundt) and btnp(down)) then
            p.crater = true
            p.dx = 0
            p.dy += 3
            p.max_dy += 4
        end
    end

    if p.run and not btn(left) and not btn(right) and p.groundt then
        p.run = false
        p.slide = true
    end

    if (p.dy > 0) then
        p.fall = true

        if (map_clisn(p, down, lyr_fall, 2)) then
            if (p.crater) then
                shake(p.dy * 2)
                sfx(sfx_crater)
            end
            p.fall = false
            p.glide = false
            p.crater = false
            p.groundt = true
            p.dy = 0
            p.max_dy = 3 -- reset from crater
            p.y -= (p.y + p.h + 1) % 8
        end
    elseif (p.dy < 0) then
        if (map_clisn(p, up, lyr_ceil, 2)) then
            p.dy = 0
        end
    end

    if (p.dx > 0 and map_clisn(p, right, lyr_wall, 0)) p.dx = 0
    if (p.dx < 0 and map_clisn(p, left, lyr_wall, 0)) p.dx = 0

    -- stop sliding
    if (p.slide) then
        if (abs(p.dx) < 0.2 or p.run) then
            p.slide = false
            p.dx = 0
        end
    end

    p.x += p.dx
    p.y += p.dy * (p.glide and p.gl or 1)

    p.x = mid(bound_l, p.x, bound_r - p.w)

    -- WIN CONDITIONS
    if (p.mana >= 100 and btnp(btn2)) then
        sword.fall = true
    end
    -- LOSE CONDITIONS
    if (p.hp <= 0) then
        setStates({"tara_lose"})
    end

    p.mana = mid(0, p.mana, 100)
end

function animate_player()
    if (not p.groundt and not p.fall) then
        p.sp = 7
    elseif (p.crater) then
        p.sp = 10
    elseif (p.glide) then
        p.sp = 11
    elseif (p.fall) then
        p.sp = 8
    elseif (p.slide) then
        p.sp = 9
    elseif (p.run) then
        -- TODO theres a single frame on landing where fall = false and run = true,
        animate(p, 3, 6, .1)
    else
        animate(p, 1, 2, .5)
    end
end

function map_clisn(ob, dir, layer, xins)
    local x, y, w, h, x1, x2, y1, y2
        = ob.x, ob.y, ob.w, ob.h, 0, 0, 0, 0
    if (dir == left) then
        x1 = x      x2 = x+1
        y1 = y      y2 = y+h
    elseif (dir == right) then
        x1 = x+w-1  x2 = x+w
        y1 = y      y2 = y+h
    elseif (dir == up) then
        x1 = x+xins x2 = x+w-xins
        y1 = y-1    y2 = y
    elseif (dir == down) then
        x1 = x+xins x2 = x+w-xins
        y1 = y+h+1  y2 = y+h+1
    end

    db_x1, db_x2, db_y1, db_y2 = x1, x2, y1, y2

    x1 /= 8 x2 /= 8
    y1 /= 8 y2 /= 8

    x1 += map_x x2 += map_x
    y1 += map_y y2 += map_y

    return fget(mget(x1, y1),layer)
    or fget(mget(x1, y2),layer)
    or fget(mget(x2, y1),layer)
    or fget(mget(x2, y2),layer)
end

function draw_player()
    spr(p.sp, p.x, p.y, 1, 1, p.flipx)
    -- DEBUG HITBOX
    -- print("p.dy "..p.dy, 0, 0)
    -- rect(db_x1,db_y1,db_x2,db_y2, blue)

    -- DEBUG FLAGS
    -- print("run "..tostr(p.run))
    -- print("slide "..tostr(p.slide))
    -- print("fall "..tostr(p.fall))
    -- print("groundt "..tostr(p.groundt))
    -- print("crater "..tostr(p.crater))
end

function draw_player_hud()
    -- hp bar
    local pct = 112 * p.hp / 100
    rectfill(10,5,10+pct,10,green)
    rect(10,5,122,10,dark_green)

    -- mana bar
    local pct = 112 * p.mana / 100
    rectfill(10,15,10+pct,20,blue)
    rect(10,15,122,20,dark_blue)

    if (p.mana >= 100) oprint("press ❎ to unleash mana!!", 15, 25+bo, pri, sec)
end

-->8
-- npcs

function init_npcs()
    -- just one for now
    npcs = {{
        name="tara",
        x=15,
        y=88,
        sp = 17,
        sps = {17,18},
        t=0,
        tracks = true,
        flipx = false,
        dia={
            -- TODO how to persist alredy-played dialogue between scenes
            {
                fr = {"tara_win"},
                r = true,
                sc = scripts.tara_win
            },
            {
                fr = {"tara_lose"},
                r = true,
                sc = scripts.tara_lose
            },
            {
                sc = scripts.tara
            },
            {
                fr = {"tut"},
                sc = scripts.tara_tut
            },
            {
                fr = {"tara2"},
                sc = scripts.tara_tara2
            },
            {
                r = true,
                sc = scripts.tara_r
            }
        }
    },
    {
        name="cat",
        x=100,
        y=80,
        sp = 35,
        sps = {35, 36},
        t=0,
        dia={
            {
                fr = {"notara"},
                sc = scripts.fight,
                r=true
            }
        }
    }
    -- , {
    --     name="tara2",
    --     x=100,
    --     y=80,
    --     sp = 17,
    --     t=0,
    --     tracks = true,
    --     flipx = false,
    --     dia={
    --         {
    --             sc = scripts.tara2
    --         }
    --     }
    -- }
    }
end

function draw_npcs()
    for npc in all(npcs) do
        spr(npc.sp, npc.x, npc.y, 1, 1, npc.flipx)

        if (npc.talk) then
            local x = npc.x + 10
            local str = "talk to "..npc.name
            if (128 - npc.x < 70) x -= 60 -- TODO use print output to position left
            local y = npc.y-8+bo

            oprint(str, x, y, pri, sec)
            oprint("❎", npc.x, y, pri, sec)
        end
    end
end

function update_npcs()
    for npc in all(npcs) do
        -- make the npc look at the player
        if (npc.tracks) npc.flipx = p.x < npc.x

        -- check interaction range
        if ((not dia.play) and abs(npc.x - p.x+3) < 15 and npc.y == p.y) then
            if (get_nx(npc)) npc.talk = true
        else
            npc.talk = false
        end

        -- TODO adjacent NPCs might double-activate, how to select
        if (npc.talk and btnp(btn2)) then
            local node = get_nx(npc)
            node.v = true
            load_scrpt(node.sc)
        end
    end
end

-- returns the next script node to load, or nil if none remaining
function get_nx(npc)
    for node in all(npc.dia) do
        if not node.v or node.r then
            local ok = true
            if (node.fr) then
                for flag in all(node.fr) do
                    if (not hasState(flag)) then
                        ok = false
                        break;
                    end
                end
            end
            if (ok) then
                return node
            end
        end
    end
end

function animate_npcs()
    for npc in all(npcs) do
        animate(npc, npc.sps[1], npc.sps[2], .5)
    end
end

-->8
-- dialogue manager

function init_dia()
    pri = pink
    sec = dark_blue
    dia = {} -- currently active dialogue set
    dia.play = false -- is the dialogue playing?
    dia.ps = {} -- the phrases to be animated thru:
        -- phrase: {sp: "the speaker", tx: "the text, 23 chars per line"}
    dia.t = 0 -- anim time
    dia.s = 1 -- anim step
    dia.pi = 1 -- sentence index
    dia.i = 0 -- char index
    dia.sel = nil -- currently selected dialog option
end

function get_sp()
    return dia.ps[dia.pi].sp
end

function get_txt()
    return dia.ps[dia.pi].txt
end

function get_opts()
    return dia.ps[dia.pi].opts
end

function get_dests()
    return dia.ps[dia.pi].dests
end

function get_flags()
    return dia.ps[dia.pi].flags
end

function is_dia_pause()
    return dia.i > #get_txt()
end

function update_dia()
    if (dia.play) then
        step_dia()

        if (is_dia_pause()) then
            if (get_opts()) then
                dia.sel = dia.sel and dia.sel or 1
                if (btnp(up)) then
                    sfx(sfx_sel)
                    dia.sel = mid(1, dia.sel-1, #get_opts())
                end
                if (btnp(down)) then
                    sfx(sfx_sel)
                    dia.sel = mid(1, dia.sel+1, #get_opts())
                end
            end

            if (btnp(btn2)) then
                sfx(dia.sel and sfx_adv_sel or sfx_adv)
                adv_dia()
            end
        end
    end
end

function load_scrpt(script) -- table of phrase strings to be split
    init_dia()

    for phrase in all(script) do
        local ph = {}
        local tbl = split(phrase, "|")
        ph.sp = tbl[1]

        -- go to max index. step back until we find a space. add a newline there, continue
        local out, min, chars, j = "", 0, split(tbl[2], ""), nil
        for i=1,#chars do
            if (chars[i] == " ") j = i -- last discovered space
            if (i > 22+min) then
                -- go back to last discovered space and swap it with a line break
                if (not (j == nil)) then
                    chars[j] = "\n"
                    min = j+1
                    j = nil
                    i = min
                end
            end
        end

        for i=1,#chars do
            out = out ..chars[i]
        end
        ph.txt = out

        local opts = split(tbl[3], ";")
        if (not (opts[1] == "")) then
            ph.opts = {}
            for opt in all(opts) do
                add(ph.opts, opt)
            end
        end

        local dests = split(tbl[4])
        if (#dests > 1) then
            ph.dests = {}
            for dest in all(dests) do
                add(ph.dests, dest)
            end
        elseif (not (dests[1] == "")) then
            ph.dests = dests[1]
        end

        local flags = split(tbl[5])
        if (flags and #flags > 0) then
            ph.flags = {}
            for flag in all(flags) do
                add(ph.flags, flag)
            end
        end

        add(dia.ps, ph)
    end

    dia.play = true
end

function step_dia()
    if (get_txt() and dia.i <= #get_txt()) then
        dia.t = step(dia.t, dia.s)
        if (dia.t == 0) then
            dia.i += 1
        end
    end
end

function adv_dia()
    -- STATE: remember state flags for the sel index
    if (get_opts()) then
        local flag = get_flags() and get_flags()[dia.sel]
        if (flag) setStates({flag})
    else
        local flag = get_flags()
        if (flag) setStates({flag[1]})
    end

    -- TODO some opts just hard exit -- handle optional dests
    if (get_dests()) then
        dia.i = 0

        if (not get_opts()) then
            dia.pi = get_dests()
        else
            dia.pi = get_dests()[dia.sel]
        end

        dia.sel = nil
    else
        dia.pi = 1
        dia.play = false
    end
end


function draw_dia()
    if (not dia.play) return

    -- calced based on t, b, l, r: 10, 50, 10, 117
    -- static dialogue box
    rectfill(11,11,116,49,sec)
    line(12,10,115,10,pri)
    line(12,50,115,50,pri)
    line(10,12,10,48,pri)
    line(117,12,117,48,pri)
    -- corner sprites
    spr(16,5,5)
    spr(16,115,5,1,1,true)
    spr(32,5,48)
    spr(32,115,48,1,1,true)
    -- speaker text
    print(get_sp() .. ":", 17, 16)
    -- text
    print(sub(get_txt(), 0, dia.i), 17, 24)

    -- if dialogue options, display them and show selection
    if (is_dia_pause()) then
        local opts = get_opts()
        if (opts and #opts > 0) then
            for i=1,#opts do
                local y = 45 + (i*10)
                oprint(opts[i], 20, y, pri, sec)
                if (i == dia.sel) spr(48, 9+bo, y-1)
            end
        else
            print("❎", 108, 42+bo)
        end
    end

end

-->8
--script

scripts = {
    fight = {
        "cat|you and tara are up next. ready to fight?|let\'s go!;not yet||fight",
    },
    nofight = {
        "cat|there are no more matches tonight.||",
    },
    tara_intro = {
        "announcer|first up, we have tara the enchantress versus val!|come at me!;...||",
    },
    tara_outro_win = {
        "announcer|and val takes the fight!||2",
        "tara|oof, that hurt.|||outro_finish",
    },
    tara_outro_lose = {
        "announcer|and tara takes the fight!||2",
        "tara|nothing\'s going to stop me this year!|||outro_finish",
    },
    tara = {
        "tara|hey there. name\'s tara.||2",
        "val|hi. i\'m val.||3",
        "tara|you\'re... not a sorceress, are you?||4",
        "val|guilty! i uh, kinda snuck into this tourney through a loophole.||5",
        "tara|huh. interesting.||6",
        "tara|first time, then?|yeah.;nah.|7,10|tut,notut",
        "tara|fresh blood! haha.||8",
        "tara|looks like i\'m your first match. but hey, you need help, just ask.||9",
        "val|thanks, i just might.||",
        "tara|cool. well, don\'t feel bad if i beat you in the first round >:)||11",
        "val|psh, you think you can handle me?||",
    },
    tara_tut = {
        "tara|ok, let me show you.||2",
        "tara|press ⬆️ to jump. press ⬆️ while falling to glide.||3",
        "tara|press ⬇️ while on the ground to slide.||4",
        "tara|press ⬇️ while in the air to smash into the ground.||5",
        "tara|got all that?|yep;uhhh...|6,2",
        "tara|glad to help.||",
    },
    tara_r = {
        "val|hey, good luck.||2",
        "tara|yeah, you too.||",
    },
    tara_win = {
        "tara|what a move... but good fight!||2",
        "tara|looks like today\'s wrapping up. wanna grab a drink?||3",
        "tara|you can tell me all about how you got here.|hell yeah;not this time|4,7",
        "val|with you? yeah, think i might.||5",
        "tara|cool, let\'s get wild.||6",
        "NARRATOR|THANKS FOR PLAYING!!!||",
        "tara|aw, too bad.||6",
    },
    tara_lose = {
        "tara|tough fight.||2",
        "tara|looks like today\'s wrapping up. wanna grab a drink?|hell yeah;not this time|3,6",
        "val|with you? yeah, think i might.||4",
        "tara|cool, let\'s get wild.||5",
        "NARRATOR|THANKS FOR PLAYING!!!||",
        "tara|aw, too bad.||5",
    }
}

__gfx__
000000000000000000222220000222220000000000022222000000000002222220022222000000000042520006666660000000000000000000000000007cc700
0000000000222220022ffff00022ffff200222220222ffff202222220022ffff0222ffff0002222220222200600000060000000000000000000000000ccccc70
00000000022ffff0022fcfc00222fcfc0222ffff2022fcfc0222ffff0022fcfc0022fcfc2222ffff222222200f22222f0000000000000000000000001ccc7cc7
00000000022fcfc0002ffff02000ffff0022fcfc0000ffff0002fcfc0020ffff0000ffff0022fcfc02ffff20042ffff40000000000000000000000001cccccc7
000000000022fff000424400004445000000ffff005544000000ffff02044400005544400000ffff02fcfc20022fcfc00000000000000000000000001ccccccc
00000000004244000f0240f00f00405f000544000f00404f00044500204045000f00400f0000440000ffff00002ffff000000000000000000000000011ccccc7
000000000f0450f00004500000054400000f4f0000044500000f4f000f054f00000544000005444f0050040000544400000000000000000000000000011cccc0
00000000004005000040050000500400000540000040050000045000005440000005040000f00445000f0f000554400000000000000000000000000000111c00
000000000000000000ddddd00dddddd0d0dddddd0000600000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000ddddd00dd44440dd4444dddd4444d00000600000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000dd444400dd434300d8448dd0d8448dd0000600000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000dd434300dd444400d4444d0dd4444dd0022222000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000dd444400ddd9900dd999901d099990d02277722000000000000000000000000000000000000000000000000000000000000000000000000000c7000
0000000e0ddd990004dd9040490990944909909422c777c220000000000000000000000000000000000000000000000000000000000000000000000000ccc700
000000e004dd904000099000000990000009900000c777c0000000000000000000000000000000000000000000000000000000000000000000000000001ccc00
00000e000009090000090900009009000090090000c777c000000000000000000000000000000000000000000000000000000000000000000000000000011000
00000e000000000000666600d000000dd000000d00c777c000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000e00066660000c66c00dd0000dd5ddddddd00c777c000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000e00c66c00006666005ddddddd05addad000c777c000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000006666000056620005addad000dddd0000c777c000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000005662000221222000dddd000005d00000cc7cc000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000002212220022dd2200005d000005ddd0d000c7c0000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000022dd22022211122005ddd0d005ddd05000ccc0000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000221112022111112005ddd55005ddd500000c00000000000000000000000000000000000000000000000000000000000000000000000000000000000
01100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1ee11000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1eeee110000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1eeeeee1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1eeee110000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1ee11000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
01100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
66666666666666660066666666666600666566666656666565666566656665664444444445444444454445444544454400000000000000000000000000000000
66666666666566660665666665665660565556566555565556565656566656564454555454445454545454545454545400000000000000000000000000000000
65655655555456650654565554555560000022000022000022000022000000000000550000550000550000550000000000000000000000000000000000000000
5454454444444554665555454444656600000c6006c0000066000026000000000000044005400000440000450000000000000000000000000000000000000000
44446444444446446544644464444456000000c66c00000066000066000000000000005554000000440000440000000000000000000000000000000000000000
444444444464444465544444444444560000000cc0000000c6000066000000000000000540000000540000440000000000000000000000000000000000000000
4444444444444444656444464444465600000000000000006600006c000000000000000000000000440000440000000000000000000000000000000000000000
4464444444444444654444444444445600000000000000006600006c000000000000000000000000440000440000000000000000000000000000000000000000
44444444444444440555555005555550000000000000000066000066000000000000000000000000440000440000000000000000000000000000000000000000
444444644644444405444450054400500000000000000000c6000066000000000000000000000000540000440000000000000000000000000000000000000000
464444444444444455444455554440550000000000000000c600006c000000000000000000000000440000450000000000000000000000000000000000000000
4444444444446444566644455464600500000000000000006600006c000000000000000000000000440000450000000000000000000000000000000000000000
44446444444444445444444556444005000000000000000066000066000000000000000000000000440000440000000000000000000000000000000000000000
44444444444444465444446554444005000000000000000066000066000000000000000000000000440000440000000000000000000000000000000000000000
444444444464444456664445546440050000000000000000c6000066000000000000000000000000440000440000000000000000000000000000000000000000
446444444444444454444445564440050000000000000000c6000066000000000000000000000000540000440000000000000000000000000000000000000000
00000000000000000000000000005555555000000000000550000000000000000000000000000000000000000000000000000000000000000000000000000000
00005000000000000000000000055555555500000000005555000000000000000000000000000000000000000000000000000000000000000000000000000000
00555500005500000000000000555555555550000000055555500000000000000000000000000000000000000000000000000000000000000000000000000000
05555555055555000000000000555555555550005000005955000000000000000000000000000000000000000000000000000000000000000000000000000000
55555555555555500000000000555555555555005500005555000550000000000000000000000000000000000000000000000000000000000000000000000000
55555555555555500000000005555555555555505550005595005555000000000000000000000000000000000000000000000000000000000000000000000000
55555555555555550555500555555555555555559555005955005595000000000000000000000000000000000000000000000000000000000000000000000000
55555555555555555555555555555555555555555550005555005555000000000000000000000000000000000000000000000000000000000000000000000000
55555555000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
55555555000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
55555555000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
55555555000000000000000000000000000000000000000500000000000000000000000000000000000000000000000000000000000000000000000000000000
55555555000000000000000000000000000000000000005500000000000000000000000000000000000000000000000000000000000000000000000000000000
55555555000000555000000000000000000000000000055500000000000000000000000000000000000000000000000000000000000000000000000000000000
55555555000005555500000000000000000000005500559500000000000000000000000000000000000000000000000000000000000000000000000000000000
55555555000055555550000000000000000000005500055500000000000000000000000000000000000000000000000000000000000000000000000000000000
__label__
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee000000000000
00000000000e11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e00000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e111111eee1eee1eee1eee1111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111e11e1e1e1e1e1e11e1111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111e11eee1ee11eee1111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111e11e1e1e1e1e1e11e1111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111e11e1e1e1e1e1e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e111111eee1eee1eee11ee1eee11111eee11ee1e1e1eee1ee11eee1e1e1eee111111111111111111111111111111111111111111111e0000000000
0000000000e111111e1111e11e1e1e1111e1111111e11e1e1e1e1e1e1e1e1e111e1e111e111111111111111111111111111111111111111111111e0000000000
0000000000e111111ee111e11ee11eee11e1111111e11e1e1e1e1ee11e1e1ee11eee11ee111111111111111111111111111111111111111111111e0000000000
0000000000e111111e1111e11e1e111e11e1111111e11e1e1e1e1e1e1e1e1e11111e1111111111111111111111111111111111111111111111111e0000000000
0000000000e111111e111eee1e1e1ee111e1111111e11ee111ee1e1e1e1e1eee1eee11e1111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
0000000000e1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e0000000000
00000000000e11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111e00000000000
000000000000eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee000000000000
000000000000000000000000000000006600006c0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000660000660000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000660000660000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000001100000001111111111111111100660000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000001ee11000001e1e1eee1eee1e1e100660000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000001eeee110001e1e1e111e1e1e1e165666566656600000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000001eeeeee1001eee1ee11eee1eee156565666565600000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000001eeee11000111e1e111e1e1e1e111110000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000001ee11000001eee1eee1e1e1e1e11e160000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000001100000001111111111111111111160000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000c60000660000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000006600006c0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000006600006c0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000001111111111111660000660000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000001ee11eee1e1e1c60000660000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000001e1e1e1e1e1e1c600006c0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000001e1e1eee1eee16600006c0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000001e1e1e1e1e1e1111000660000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000001e1e1e1e1e1e11e1000660000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000001111111111111111000660000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000c60000660000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000006665666665666566665666656566656665666566665666650000000000000000000000000000000000000000000000000000000000000000
00000000000000005655565656565656655556555666565656565656655556550000000000000000000000000000000000000000000000000000000000000000
00000000000000000000110011000011001100000000000011000011001100000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000c606600001606c00000000000006600001606c000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000c6660000666c00000000000000660000666c0000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000cc6000066c000000000000000c6000066c00000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000006600006c00000000000000006600006c000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000006600006c00000000000000006600006c000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000066000066000000000000000066000066000000000000000000000000000000000000000000000555555000000000000000000000
000000000000000000000000c60000660000000000000000c6000066000000000000000000000000000000000000000000000544445000000000000000000000
000000000000000000000000c600006c0000000000000000c600006c000000000000000000000000000000000000000000005544445500000000000000000000
0000000000000000000000006600006c00000000000000006600006c000000000000000000000000000000000000000000005666444500000000000000000000
00000000000000000000000066000066000000000000000066000066000000000000000000000000000000000000000000005444444500000000000000000000
00000000000000000000000066000066000000000000000066000066000000000000000000000000000000000000000000005444446500000000000000000000
000000000000000000000000c60000660000000000000000c6000066000000000000000000000000000000000000000000005666444500000000000000000000
000000000000000000000000c60000660000000000000000c6000066000000000000000000000000000000000000000000005444444500000000000000000000
00000000000000000111110066000066000000000000000066000066000000000000000000000000000000000000000000666666666666000000000000000000
000000000000000011444400c60022222000000000000000c6000066000000000000000000000000000000000000000006656666656656600000000000000000
000000000000000011434300c600ffff2200000000000000c600006c000000000000000000000000000000000000000006515655515555600000000000000000
0000000000000000114444006600cfcf22000000000000006600006c000000000000000000000000000000000000000066555515111165660000000000000000
0000000000000000111990006600fff2200000000000000066000066000000000000000000000000000000000000000065116111611111560000000000000000
00000000000000004119040066000442400000000000000066000066000000000000000000000000000000000000000065511111111111560000000000000000
000000000000000000990000c600f0540f00000000000000c6000066000000000000000000000000000000000000000065611116111116560000000000000000
000000000000000000909000c60005664000000000000000c6000066000000000000000000000000000000000000000065111111111111560000000000000000
66656666656665666656666565666566666566666566656666566665000000000000000000000000000000000000000011111111111111110000000000000000
56555656565656566555565556665656565556565656565665555655000000000000000000000000000000000000000011111161111111610000000000000000
00001100110000110011000000000000000011001100001100110000000000000000000000000000000000000000000016111111161111110000000000000000
00000c606600001606c000000000000000000c606600001606c00000000000000000000000000000000000000000000011111111111111110000000000000000
000000c6660000666c00000000000000000000c6660000666c000000000000000000000000000000000000000000000011116111111161110000000000000000
0000000cc6000066c0000000000000000000000cc6000066c0000000000000000000000000000000000000000000000011111111111111110000000000000000
000000006600006c0000000000000000000000006600006c00000000000000000000000000000000000000000000000011111111111111110000000000000000
000000006600006c0000000000000000000000006600006c00000000000000000000000000000000000000000000000011611111116111110000000000000000
00000000660000660000000000000000000000006600006600000000000000000000000000000000006666666666666611111111111111116666660000000000
00000000c6000066000000000000000000000000c600006600000000000000000000000000000000066566666666666611111161111111616566566000000000
00000000c600006c000000000000000000000000c600006c00000000000000000000000000000000065156556565565516111111161111115155556000000000
000000006600006c0000000000000000000000006600006c00000000000000000000000000000000665555155151151111111111111111111111656600000000
00000000660000660000000000000000000000006600006600000000000000000000000000000000651161111111611111116111111161116111115600000000
00000000660000660000000000000000000000006600006600000000000000000000000000000000655111111111111111111111111111111111115600000000
00000000c6000066000000000000000000000000c600006600000000000000000000000000000000656111161111111111111111111111111111165600000000
00000000c6000066000000000000000000000000c600006600000000000000000000000000000000651111111161111111611111116111111111115600000000
00000000660000660000000000000000000000006600006600000000006666666666660000000000111111111111111111111111111111111111111166666600
00000000c6000066000000000000000000000000c600006600000000066566666566566000000000111111611111116111111161111111611111116165665660
00000000c600006c000000000000000000000000c600006c00000000065156555155556000000000161111111611111116111111161111111611111151555560
000000006600006c0000000000000000000000006600006c00000000665555151111656600000000111111111111111111111111111111111111111111116566
00000000660000660000000000000000000000006600006600000000651161116111115600000000111161111111611111116111111161111111611161111156
00000000660000660000000000000000000000006600006600000000655111111111115600000000111111111111111111111111111111111111111111111156
00000000c6000066000000000000000000000000c600006600000000656111161111165600000000111111111111111111111111111111111111111111111656
00000000c6000066000000000000000000000000c600006600000000651111111111115600000000116111111161111111611111116111111161111111111156
66666666666666666666666666666666666666666666666666666666111111111111111166666666111111111111111111111111111111111111111111111111
66666666666566666666666666656666666666666666666666656666111111611111116166666666111111611111116111111161161111111111116111111161
65655655555156656565565555515665656556556565565555515665161111111611111165655655161111111611111116111111111111111611111116111111
51511511111115515151151111111551515115115151151111111551111111111111111151511511111111111111111111111111111161111111111111111111
11116111111116111111611111111611111161111111611111111611111161111111611111116111111161111111611111116111111111111111611111116111
11111111116111111111111111611111111111111111111111611111111111111111111111111111111111111111111111111111111111161111111111111111
11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111116111111111111111111111
11611111111111111161111111111111116111111161111111111111116111111161111111611111116111111161111111611111111111111161111111611111

__gff__
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007070707010101010101010100000000070700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__map__
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000474446454700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000056000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000056000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000004746470000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000056000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000444645474645000000000000000000000000004746464646470000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000005600005600000000000000000000000000000056565656000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000005600005600000000004243000000000000000056565656000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
4446454744464500000000005050000000444645000056565656000044464500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0056000000560000000042405050430000005600000056464656000000560000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0056000000560042430050505050504300005600000056565656000000560000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
4041404140404150504050505051505040414040414041414140404040414040000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000063640000716061720000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
7565667565666370706261637070647500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
5050505050505050505050505050505000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__sfx__
00050000150501a0501a0503400035000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00030000270202e0200f6000f60000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00030000350203c0200f6000d6000c6000a6000960009600086000760007600066000560004600036000360002600026000160001600016000000000000000000000000000000000000000000000000000000000
000200002005024050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000400000f6500d6500b6500964008640076400662005620046100361002610016000160000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0003000018030110200a0200805006050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000300000b120141101d1200f6001c600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000200001f7501a750147500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00040000256502c650306603366035660376603766037660356603465032650306502e6502c6502a6402764024640206301d6301b630136301262012620126201362012630116300f6200b610096100761004610
